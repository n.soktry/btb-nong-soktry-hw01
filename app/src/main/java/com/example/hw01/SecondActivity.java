package com.example.hw01;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        TextView textView = (TextView) findViewById(R.id.textView);
        Button button = (Button) findViewById(R.id.button);
        final EditText editText = (EditText) findViewById(R.id.editText);

        int imageID = getIntent().getIntExtra("imageID", 1);
        if (imageID == 1) {
            imageView.setImageResource(R.drawable.dodo);
            textView.setText(R.string.dodo);
        } else if (imageID == 2) {
            imageView.setImageResource(R.drawable.puffin);
            textView.setText(R.string.puffin);
        }

        final Intent intent = getIntent();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = editText.getText().toString();
                if (message.isEmpty()){
                    intent.putExtra("message", "You have sent an empty message!");
                }else {
                    intent.putExtra("message", message);
                }
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}