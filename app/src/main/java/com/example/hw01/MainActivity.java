package com.example.hw01;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageDodo = (ImageView) findViewById(R.id.imageDodo);
        ImageView imagePuffin = (ImageView) findViewById(R.id.imagePuffin);

        imageDodo.setOnClickListener(this);
        imagePuffin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int imageID = 0;
        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        switch (view.getId()) {
            case R.id.imageDodo:
                imageID = 1;
                break;
            case R.id.imagePuffin:
                imageID = 2;
                break;
        }
        intent.putExtra("imageID", imageID);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        String message = intent.getStringExtra("message");
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}